#ifndef CONTAINERWIDGET_H
#define CONTAINERWIDGET_H

#include "sourcesmanagerwidget.h"
#include "plotwidget.h"
#include "frequencymanagerwidget.h"
#include "sourcestablemodel.h"
#include "simulatorwindow.h"

#include <QWidget>
#include <QVBoxLayout>

class ContainerWidget : public QWidget
{
    Q_OBJECT
    friend class SimulatorWindow;
    //friend void SimulatorWindow::compute();
protected:
    SourcesTableModel *model;
    QVBoxLayout *mainLayout;
    SourcesManagerWidget *sourcesManager;
    PlotWidget *plotWidget;
    FrequencyManagerWidget *frequencyManager;


public:
    explicit ContainerWidget(QWidget *parent = nullptr);
    ~ContainerWidget();
    void setModel(SourcesTableModel *newModel);

//signals:

};

#endif // CONTAINERWIDGET_H
