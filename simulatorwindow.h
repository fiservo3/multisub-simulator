#ifndef SIMULATORWINDOW_H
#define SIMULATORWINDOW_H

#include "solver.h"
#include "sourcestablemodel.h"
#include "containerwidget.h"

#include <QMainWindow>

class SimulatorWindow : public QMainWindow
{
    Q_OBJECT
    //friend class ContainerWidget;
private:
    MultisubSolver *solver;
    SourcesTableModel *model;
    class ContainerWidget *container;
protected slots:
    void compute();

public:
    SimulatorWindow(QWidget *parent = nullptr);
    ~SimulatorWindow();
};
#endif // SIMULATORWINDOW_H
